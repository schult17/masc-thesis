JOB = tannerys_masc_thesis
PDF = $(JOB).pdf

MAINSRC = $(JOB).tex
SRC = $(MAINSRC) $(JOB).bib

# All the targets.
.PHONY: all view remove clean

# Compiles the PDF file using the TEX files.
all : $(PDF)

# Open uses default PDF viewer to open the PDF.
view: FORCE
	#@open $(PDF) || xdg-open $(PDF)
	@cmd /C start Build/$(PDF)
    
# Remove deletes auxillary files and the  PDF output file.
remove: FORCE
	@latexmk -C -output-directory=./Build

# Clean removes auxillary files, shouldn't be needed - but just incase.
clean: FORCE
	@latexmk -c -output-directory=./Build

# PDF dependancies.
$(PDF): FORCE
	@latexmk -output-directory=./Build -pdf $(MAINSRC)

# Force latexmk to run, which keeps track of timestamps
FORCE:
