\chapter{Conclusions and Future Work} \label{chp:conclusions_and_future_work}
This chapter summarizes the contributions of this thesis and gives suggestions for future research to extend this work.

\section{Conclusions} \label{sec:conclusions_and_future_work:conclusions}
The main contributions of this thesis advance the field of hardware accelerated biophotonic simulators by creating the fastest GPU-accelerated simulator and first complete and verified FPGA-accelerated simulator of their kind. These accelerators are fully integrated into the full-featured FullMonteSW simulator, so they can be used with all the features of that software, while accelerating the most CPU-intensive calculations. In addition, this thesis further enhanced FullMonteSW by making other improvements to its performance, accuracy and usability.

Our addition of the cylindrical diffuser and mesh region light sources makes the simulator more accurate in representing the light propagation in PDT treatment and BLI, respectively. In the future, the addition of new light sources and processing techniques could extend the applicability of the simulator to other applications in the medical field and even to applications outside of biophotonics where objects need to be located in turbid media. Examples of potential applications include autonomous vehicles navigating through light scattering fog or aquatic navigation when diving close to the seabed \cite{mc_fog,mc_deep_sea}. The RTree data structure that we implemented in FullMonteSW improved the point to containing tetrahedron query speed by over 230x. This query can happen millions of times in a single forward simulation and even more when solving inverse problems. This query is not part of the key hop-drop-spin loop that we hardware accelerate, and therefore its software performance can be vital to the performance of the simulator and to inverse solvers which use it.

To this date, the GPU-accelerated simulator that we created, FullMonteCUDA, is the fastest tetrahedral-mesh Monte Carlo biophotonic simulator. FullMonteCUDA is completely integrated with the FullMonteSW code, which allows the simulation acceleration to be transparent to the user. FullMonteCUDA provides a 4-13x speed improvement over the fastest software simulator for this problem (FullMonteSW), which can enable inverse solvers for complex medical applications, like DOT, BLI and PDT, to be more accurate and feasible.

We also explored FPGAs as a means for hardware acceleration and developed FullMonteFPGACL: the first complete and verified FPGA-accelerated simulator of its kind. FullMonteFPGACL achieved up to a 4x speed improvement over FullMonteSW and an 11x improvement in energy-efficiency. Similar to FullMonteCUDA, FullMonteFPGACL is completely integrated with the FullMonteSW code, which allows the acceleration to be transparent to the user. While FullMonteFPGACL trails FullMonteCUDA in performance, we only use a fraction (\til{}20\%) of the FPGA area for the two pipelines. In the next section, we discuss an outline for future work that could remove its current constraint of storing at most 65k tetrahedra and scale up the performance of the FPGA design to achieve a 16x speed improvement and 17x energy efficiency improvement over FullMonteSW, which would exceed the performance of FullMonteCUDA.

\section{Future Work} \label{sec:conclusions_and_future_work:future_work}
In this section we will discuss methods for improving the performance and usability of the FPGA and GPU accelerators from this work.

\subsection{FPGA} \label{sec:conclusions_and_future_work:future_work:fpga}
This section will discuss methods for reducing the FPGA resource utilization, removing the mesh element size constraint and scaling up the design to increase performance.

\subsubsection*{Fixed-point Representation}
Our design uses 32-bit precision integers (i.e. \texttt{int} and \texttt{uint}) for data not requiring decimal point precision (e.g. tetrahedron and material IDs) and 32-bit precision floating point numbers (i.e. \texttt{float}) for all data entries that require decimal point precision (e.g. positions, vectors, material properties, lengths, etc), with the exception of the 64-bit fixed-point energy accumulation that was discussed in Section \ref{sec:hardware_acceleration:fpga:design}. We performed software profiling (details of which can be found in \cite{cassidy_thesis}) to determine the range and precision needs for different variables and the results are summarized in Table \ref{tab:fpga_data_precision}. Future work could include using the \textit{arbitrary integer precision extension} of the Intel FPGA SDK for OpenCL \cite{intel_fpga_opencl_sdk_programming_guide} which allows the OpenCL design to easily use custom width and precision fixed-point integers. This could result in up to a 2x reduction in RAM, DSP and ALM utilization for the OpenCL kernels. This reduction in resources would allow for more throughput per device by instantiating even more pipelines on the device, as discussed later in this section.

\begin{table}[!htb]
    \centering
    \caption{FPGA precisions for various data determined by software emulation \cite{cassidy_thesis}}
    \label{tab:fpga_data_precision}
    \begin{tabular}{l|l|l|l|l|l}
        \hline
        Data & Total Bits & Integer bits & Fractional bits & Range & Precision \\
        \hline
        3D Unit Vector & 18 & 1 & 17 & [-1, 1] & \num{8e-6} \\
        3D Position & 18 & 4 & 14 & [-8cm, 8cm] & \num{6e-5}cm \\
        Step length & 18 & 6 & 12 & [0, 63] & \num{1.2e-4} \\
        Packet weight & 36 & 1 & 35 & [0, 1] & \num{3e-11} \\
        Tetrahedral absorption & 64 & 29 & 35 & [0, \num{2e8}] & \num{3e-11} \\
        Tetrahedral ID & 25 & 25 & --- & [0, \num{3e7}] & --- \\
        Material ID & 4 & 4 & --- & [0, 15] & --- \\
        \hline
    \end{tabular}
\end{table}

\subsubsection*{Custom Memory Controller}
Our design limits the size of the mesh to 65k tetrahedral elements. However, practical medical models can contain more than 1 million tetrahedrons. The storage of data is broken down into two components: read-only tetrahedral geometry data and read-write energy accumulation array data. We profiled the memory access pattern and found that there is little temporal reuse of data and that a 4-8 entry LRU cache for both the geometry and accumulation arrays could provide a hit-rate near 60\%. Larger LRU caches show poor trade-offs in terms of area and hit-rate increase. As hypothesized, the memory profiling results across various benchmark models showed highly irregular access patterns. This access pattern does not fit the typical cache architecture and eviction policies found in most CPUs and GPUs. However, we found that, across all benchmarks, the access pattern has a Zipf-like distribution \cite{zipf}. This means that a high percentage of accesses are to a relatively small portion of the tetrahedrons.

In future work, the geometry and energy accumulation array data could be moved to the high-capacity external memory of the board and the controller implemented using the FPGA RAMs. For both the geometry and energy accumulation data, we would create a 4-8 entry LRU L1 cache backed by a static L2 cache. To populate the L2 cache, we would simulate a small subset of packets pre-emptively, sort the tetrahedrons based on the Zipf distribution (i.e. by access rate) and store the highest accessed tetrahedrons in the static L2 cache. The overhead of running a subset of packets pre-emptively in this manner would decrease the performance of a single simulation. However, this time could be amortized when running many simulations with the same mesh, which is the typical case when solving PDT inverse problems \cite{pdt-space2}.

\subsubsection*{Multiple Pipeline Instances}
In Section \ref{sec:hardware_acceleration:fpga:design}, we discussed how we instantiated two instances of the pipeline in the FPGA. This section will discuss a more sophisticated approach that can utilize the memory controller discussed in the previous section and decrease the FPGA RAM utilization when scaling the design up even further.

Based on the resource utilization from Table \ref{tab:fpga_resources}, the limiting factor for duplicating the pipeline is RAM (50\%). However, without the on-chip caching of tetrahedrons, the RAM usage of the kernels drops to \til{}10\% per pipeline and the limiting factor becomes ALMs (12\%). Thus, we believe that 8 copies of the pipeline can fit on-chip, if the RAMs are managed properly.

A naive approach would be to duplicate the on-chip memory for each instance of the pipeline, as we did for the current design. However, as the number of pipelines increases, the duplicated memory will severely limit the amount of tetrahedrons that can be stored on chip. In this trivial approach, assuming that the memory controller from the previous section is implemented, the L1 and L2 caches for both the geometry and energy accumulation data would be duplicated. This would decrease the maximum size of the static L2 caches for the memory controllers and therefore reduce the overall hit-rate. However, by utilizing the two ports and multi-pumping support of the FPGA RAMs \cite{intel_fpga_opencl_sdk_best_practices_guide, intel_fpga_opencl_sdk_programming_guide}, we can share a single memory across multiple pipelines, thereby reducing the RAM utilization per pipeline and increasing the size of the memories.

The energy accumulation arrays are read from and written to, meaning that the RAMs used to implement their cache must have one port for reading and the other for writing. In addition, the current Intel FPGA OpenCL documents \cite{intel_fpga_opencl_sdk_best_practices_guide, intel_fpga_opencl_sdk_programming_guide} only mention multi-pumping support for memory reads. Therefore, the on-chip memory controller cache for the energy accumulation arrays must be duplicated for each pipeline instance.

The tetrahedral geometry data is read-only, meaning that multiple pipeline instances can share the two read ports of the RAMs. In addition, each read port can be double or triple-pumped, allowing a single cache to supply 2, 4 or 6 instances of the pipeline if configured using no multi-pumping, double-pumping or triple-pumping, respectively. We hypothesize that we could fit 8 pipelines in the FPGA and therefore the most logical configuration for this would use two copies of the static L2 cache and double-pump the two read ports, as depicted in Figure \ref{fig:custom_cache}. This configuration would allow the static L2 geometry caches to store \til{}4x more tetrahedrons than the naive configuration.

\begin{figure}[!htb]
    \centering
    \includegraphics[width=0.8\columnwidth]{figures/custom_cache.PNG}
    \caption{Proposed memory structure for duplicating FullMonteCL pipelines \cite{fullmontefpgacl}}
    \label{fig:custom_cache}
\end{figure}

To estimate the performance of this design, we scale up the single-pipeline performance by a factor of 8, which is similar to the performance scaling we experienced with 2 pipelines (Section \ref{sec:hardware_acceleration:fpga:results}) as well as the use of more CPU cores \cite{ecbo_paper}. To estimate the energy-efficiency of this design, we multiply the core dynamic power (excluding I/O) by a factor of 8. This estimate is conservative since the global board control logic and the shared on-chip RAM caches (and therefore their dynamic power) will not scale linearly with the number of pipeline instances. This leads to an estimated 16x performance and 17x energy-efficiency improvement over FullMonteSW and a 1-4x performance and 7x energy-efficiency improvement over FullMonteCUDA.

\subsection{GPU} \label{sec:conclusions_and_future_work:future_work:gpu}
After creating the high performance GPU design, we used the \textit{NVIDIA Visual Profiler} (NVVP) to identify bottlenecks in the code. Fig \ref{fig:golden_chart} shows the profiling results for our design using the \textit{HeadNeck} mesh from Table \ref{tab:models} using \num{e6} packets and a single isotropic point source. The chart in Fig \ref{fig:golden_chart} shows the distribution of stall reasons for our kernel and lets us pinpoint the performance bottlenecks \cite{nvvp_user_guide}. The chart shows that the largest number of stalls in our optimized design are due to memory dependencies, which is typical for applications like ours with large datasets and unpredictable memory access patterns.

As we hypothesized, the tetrahedron intersection calculation was a major bottleneck consisting of mostly memory dependencies. All memory dependency bottlenecks occur when accessing tetrahedral data. Future work could investigate more advanced tetrahedron caching schemes in shared or constant memory. For example, in the previous section we discussed that the tetrahedron access pattern has a Zipf access pattern. Therefore, implementing a Zipf cache in shared or constant memory could reduce this global memory bottleneck for tetrahedrons. Prefetching tetrahedrons may also decrease the global memory bottleneck and improve the performance. When a packet resides in a tetrahedron, we know it will move to one of the four adjacent tetrahedrons on the next step. Therefore, every time a packet moves to a new tetrahedron, the kernel could prefetch the data of the four adjacent tetrahedrons while other calculations are being performed. If the packet moves to the next tetrahedron, the request for the tetrahedron memory will already be in-flight (or finished) and the global memory access latency may be reduced.
