\chapter{Software Optimizations} \label{chp:software_optimizations}
This section discusses additions and optimizations made to the existing software simulator, FullMonteSW, that improve its performance, accuracy and usability. The first section discusses two new light sources that make the simulator more accurate at representing the real light sources used in PDT and BLI. The second section discusses an optimization technique that improves the performance of the simulator and common post-processing queries made by applications that use the simulator to perform more complicated tasks, like PDT treatment planning.

\section{Representing Medical Light Sources in Simulation} \label{sec:software_optimizations_light_sources}
As discussed in Section \ref{sec:background:pdt}, PDT treatment planning often uses complex light sources, like cut-end fibers and cylindrical diffusers. In BLI, the source of light is a collection of light-emitting cells, which translates to a region of elements in the 3D mesh. Representing these real light sources in simulation requires non-trivial mathematics and intelligent programming to maintain high performance. The following sections discuss how we implement these light sources and their respective use-cases.

\subsection{Cylindrical Diffuser Light Source}
In IPDT, cylindrical diffusers are commonly used to produce light. The cladding of the fiber optic probe is stripped away such that light emits from the surface of the cylindrical probe but not out of the ends, as shown in Fig \ref{fig:sources}. This process is not perfect, which causes light to emit with a higher probability normal to the surface and the probability shrinks as the angle becomes more parallel to the surface. This is represented by a Lambertian distribution, as shown in Fig \ref{fig:theta_angle_dist}. To increase the accuracy of PDT treatment planning, it is important to accurately represent these cylindrical diffusers in simulation. To achieve this, we developed code for a cylinder light source that emits light similarly to the cylindrical diffusers. The cylinder light source is defined by the endpoints, radius, power and \textit{theta distribution}. The theta distribution is the range of angles, with respect to the cylinder surface, that an emitted photon can have. The user has three options, as depicted in Fig \ref{fig:theta_angle_dist}: normal to the surface, a uniform distribution on the unit hemisphere or a Lambertian distribution. Fig \ref{fig:q_matrix} illustrates how we randomly generate a vector in a hemisphere centered on an arbitrary plane (a 2D representation is shown, but the concepts are the same for 3D). Algorithm \ref{alg:cylinder_source} describes how the cylinder source randomly emits photon packets. Fig \ref{fig:cylinder_source} illustrates the emission profile of the cylinder source using a surface normal emission (top) and Lambertian emission (bottom). We created these images by placing a cylindrical light source in a homogenous cube with high absorption and low scattering; the darker red represents a higher fluence.

\begin{figure}[htb]
    \centering
    \includegraphics[width=0.9\textwidth]{figures/theta_angle_distribution.png}
    \caption{Planar theta angle distribution options for surface light sources}
    \label{fig:theta_angle_dist}
\end{figure}

\begin{figure}[htb]
    \centering
    \includegraphics[width=\textwidth]{figures/q_matrix.PNG}
    \caption{Generating a random vector ($\uvec{d}$) in a hemisphere centered on the z-axis (left) and rotating this vector to be centered on a plane's ($F$) normal (middle and right).}
    \label{fig:q_matrix}
\end{figure}

\noindent\begin{minipage}{\textwidth}
    \begin{algorithm}[H]
        \caption{Emitting Light from Virtual Cylindrical Diffuser}
        \label{alg:cylinder_source}

        \DontPrintSemicolon
        \KwData{The cylinder information}
        \KwResult{An initial position $\pos{p}$ and direction $\uvec{d}$ for the packet}

        $\pos{p} \gets $ randomly choose a point on surface of the cylinder (excluding the circular ends)\;
        $\uvec{c} \gets $ the normal vector at point $\pos{p}$ on the cylinder surface\;

        \uIf {theta distribution is NORMAL} {
            $\uvec{d} \gets \uvec{c}$\;
        }
        \uElse {
            \tcp{this section is illustrated in Fig \ref{fig:q_matrix}}
            \tcp{generate $\uvec{d}'$ assuming the plane normal is oriented on the positive z-axis}
            $\phi \gets U_{0,2\pi}$\;
            \uIf{theta distribution is HEMISPHERE}{
                $\theta \gets \arccos{U_{-1,1}}$\;
            }
            \uElseIf{theta distribution is LAMBERTIAN}{
                $\theta \gets \arccos{\sqrt{U_{0,1}}}$\;
            }
            \tcp{convert to cartesian coordinates. Absolute value of z-component gives a hemisphere rather than a sphere}
            $\uvec{d}' \gets (sin\theta{}sin\phi{}, sin\theta{}cos\phi{}, |cos\theta{}|)$\;
            \tcp{Rotate the hemisphere (and the random point) by centering the z-axis along the cylinder surface normal}
            $\uvec{d} \gets $ rotate $\uvec{d}'$ vector to align with $\uvec{c}$\;
        }
    \end{algorithm}
\end{minipage}

\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{figures/cylinder_sources_total.PNG}
    \caption{The side (left) and top (right) view of a virtual cylinder source with surface normal emission (top) and Lambertian emission (bottom)}
    \label{fig:cylinder_source}
\end{figure}

\subsection{Mesh Region Light Source}
As discussed in Section \ref{sec:background:bli}, BLI involves transfecting a group of diseased cells, like a tumor, with a virus that alters their genes and causes them to emit light. Since the transfected cells are the sources of light and also part of the mesh (a region or regions in the mesh), supporting BLI requires creating an arbitrary light source from volumetric regions of the mesh. In IPDT, light sources are implanted into the patient and therefore, in reality, have their own region in the mesh with optical properties that can affect the light propagation. The sources in Section \ref{sec:background:pdt_light_sources} passively emit light in the simulation; they are not part of the geometry and therefore do not affect the propagation of the light once it is emitted. Therefore, to explore the effect of light emitters with their own region and optical properties, it is necessary to include them as regions in the mesh and emit light from the volume or surface of that region.

A 2D illustration of the mesh region light source is shown in Fig \ref{fig:mesh_region_source}. The user selects the region of the mesh to emit light from and we perform pre-processing steps to determine the tetrahedrons that belong to that region and which have faces on the surface of the region. For example, in Fig \ref{fig:mesh_region_source} four faces of the triangles are on the surface of the square shaded region. The user can choose whether to emit light from only the surface of the region (Fig \ref{fig:mesh_region_source}, middle) or from within the region (Fig \ref{fig:mesh_region_source}, right). If the user chooses to emit from the surface of the region, they can choose one of the three theta angle distributions discussed in the previous section, as shown in Figure \ref{fig:theta_angle_dist}. Algorithm \ref{alg:volume_source} describes how we emit light isotropically from the volume of a region in the mesh, while Algorithm \ref{alg:surface_source} describes the more complicated method of emitting light from the surface of a region using different theta angle distributions. Fig \ref{fig:mesh_source} shows an example of the mesh region light source. The left image shows the two regions in the mesh (grey cylinder within a red square) and the right image shows the resulting fluence distribution using a method similar to that in the previous section (high absorption in the surrounding square).

\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{figures/mesh_region_source.png}
    \caption{Illustration of a mesh region light source with the mesh on the left, the isolated surface light source in the middle and the isolated volume light source on the right}
    \label{fig:mesh_region_source}
\end{figure}

\noindent\begin{minipage}{\textwidth}
    \begin{algorithm}[H]
        \caption{Emitting from Mesh Region Volume}
        \label{alg:volume_source}

        \DontPrintSemicolon
        \KwData{List of tetrahedrons in region $R$}
        \KwResult{An initial position $\pos{p}$ and direction $\uvec{d}$ for the packet}

        $T \gets$ randomly choose a tetrahedron in the region $R$ distributed by volume\;
        $\pos{p} \gets $ generate a random point inside the volume of $T$\;
        $\uvec{d} \gets $ generate a direction vector with isotropic distribution\;
    \end{algorithm}
\end{minipage}

\noindent\begin{minipage}{\textwidth}
    \begin{algorithm}[H]
        \caption{Emitting from Mesh Region Surface}
        \label{alg:surface_source}

        \DontPrintSemicolon
        \KwData{List of tetrahedrons in region $R$}
        \KwResult{An initial position $\pos{p}$ and direction $\uvec{d}$ for the packet}

        $F \gets $ randomly choose a tetrahedral face on the region $R$ distributed by surface area\;
        $\pos{p} \gets $ generate a random point on $F$\;

        \uIf {theta distribution is NORMAL} {
            $\uvec{d} \gets $ the normal vector of $F$\;
        }
        \uElse {
            \tcp{this section is illustrated in Fig \ref{fig:q_matrix}}
            \tcp{generate $\uvec{d}'$ assuming the plane normal is oriented on the positive z-axis}
            $\phi \gets U_{0,2\pi}$\;
            \uIf{theta distribution is HEMISPHERE}{
                $\theta \gets \arccos{U_{-1,1}}$\;
            }
            \uElseIf{theta distribution is LAMBERTIAN}{
                $\theta \gets \arccos{\sqrt{U_{0,1}}}$\;
            }
            \tcp{convert to cartesian coordinates. Absolute value of z-component gives a hemisphere rather than a sphere}
            $\uvec{d}' \gets (sin\theta{}sin\phi{}, sin\theta{}cos\phi{}, |cos\theta{}|)$\;
            \tcp{Rotate the hemisphere (and the random point) by centering the z-axis along the cylinder surface normal}
            $\uvec{d} \gets $ rotate $\uvec{d}'$ vector to align with the normal vector of $F$\;
        }
    \end{algorithm}
\end{minipage}

\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{figures/mesh_region_light_source_total.PNG}
    \caption{A mesh region light source with the regions (left) and the emission profile using a surface normal distribution (right)}
    \label{fig:mesh_source}
\end{figure}


\section{Improving the Performance of Tetrahedral Mesh Queries}
A common operation in both the light propagation simulation itself and post-processing queries for applications like PDT treatment planning involves determining which tetrahedron element in the mesh contains a given point $\pos{p}$. For example, finding the tetrahedron containing the random point generated on or in the 3D volume of the cylindrical diffuser described in Section \ref{sec:software_optimizations_light_sources} is necessary to launch a photon packet in simulation. These operations can be expensive, especially for real medical applications with large meshes. In this work, we optimized this query for the general case by using an RTree data structure. The following section provides a brief discussion on how this data structure works and how it is used in the context of our work to improve the performance of the simulator.

\subsection{Point to Containing Tetrahedron Lookup using RTrees}
The goal of the query is simple: given a point $\pos{p}$ inside the boundaries of the mesh, find the tetrahedron that contains it. Given a tetrahedron $T$, the normal vector of each of its faces $\uvec{n}_i$ (which by convention, are directed towards the inside of the tetrahedron) and a constant point $\pos{c}_i$ anywhere on each face, we can determine if $\pos{p}$ is within $T$ using Algorithm \ref{alg:point_in_tet}. The height $h_i$, is the distance from 2D plane made by the $i^{th}$ tetrahedral face to the point $\pos{p}$. Since each face normal vector $\uvec{n}_i$ points towards the inside of the tetrahedron, if the distance ($h_i$) from the face to the point $\pos{p}$ is positive for all faces, we know the point is within the tetrahedron. Contrarily, if the distance is negative for any face, then the point is outside of the tetrahedron. An example of both cases is illustrated in Fig \ref{fig:tetraenclose} for a single face of a triangle.

\noindent\begin{minipage}{\textwidth}
    \begin{algorithm}[H]
        \caption{Check if point is inside tetrahedron}
        \label{alg:point_in_tet}

        \DontPrintSemicolon
        \KwData{Tetrahedron $T$ defined by face normals and constants $(\uvec{n}_i,\pos{c}_i)$ and query point $\pos{p}$}
        \KwResult{Whether $\pos{p}$ is within the tetrahedron $T$}

        \For{$i \gets 0$ \KwTo $3$}{
            $h_i \gets \uvec{n}_i \cdot (\pos{p} - \pos{c}_i)$\;
        }

        \Return true if $h_i \geq 0, \forall i \in [0,3]$, otherwise false\;

    \end{algorithm}
\end{minipage}

\begin{figure}[!htb]
    \centering
    \includegraphics[width=\textwidth]{figures/tetraenclosing.PNG}
    \caption{Plane-to-point distance calculation for a point inside (left) and outside (right) of a triangle.}
    \label{fig:tetraenclose}
\end{figure}

The trivial solution for finding the tetrahedron containing a point is to check every tetrahedron in the mesh, performing Algorithm \ref{alg:point_in_tet} for each. This query can be performed millions of times during a simulation (e.g. every time a new photon packet is launched) and also when implementing more complicated applications like light source placement in PDT treatment planning. In this work, we significantly improved the performance of this query by using an \textit{RTree} datastructure.

The RTree datastructure hierarchically partitions a set of objects to reduce search time. In our case, the \textit{space} is $\mathbb{R}^3$, over which we wish to partition the tetrahedrons. This changes the time complexity of the query from linear ($O(n)$) to logarithmic ($O(logn)$), where n is the number of tetrahedra in the mesh; that is, we only consider a logarithmic number of tetrahedrons, rather than checking every tetrahedron. An example RTree structure for $\mathbb{R}^2$ is shown in Fig \ref{fig:rtree}. The left figure shows the spatial view of the partitioning, while the right figure shows an instance of the RTree datastructure. If we wish to find the object containing some point, we move down the tree on the right of Fig \ref{fig:rtree} and check if the point is contained in each of the partitions until we find the object (or a set of possible objects) that \textit{could} contain the point ($L$ in Algorithm \ref{alg:rtree_query}). For example, if the query point is the star in Fig \ref{fig:rtree}, the RTree would return the list $L = \{C,E\}$, since the point is contained in both rectangles. In our application, $L$ contains all of the tetrahedra whose bounding boxes contain the query point. However, being contained in the bounding box of a tetrahedron does not mean the point is in the tetrahedron itself. To find the tetrahedron that contains the query point, we do a linear search over the relatively small (\til{}10-100 tetrahedra) list $L$ and determine which tetrahedron contains the point using Algorithm \ref{alg:point_in_tet}. Algorithm \ref{alg:rtree_query} summarizes how we use the RTree to find the tetrahedron containing point $\pos{p}$.

\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{figures/rtree.png}
    \caption{2D object partition (left) and corresponding RTree (right)}
    \label{fig:rtree}
\end{figure}

\noindent\begin{minipage}{\textwidth}
    \begin{algorithm}[H]
        \caption{RTree Query}
        \label{alg:rtree_query}

        \DontPrintSemicolon
        \KwData{Set of tetrahedrons $T$, RTree $R$ and query point $\pos{p}$}
        \KwResult{The tetrahedron containing $\pos{p}$}

        $L \gets $ query $R$ for point $\pos{p}$\;
        $t \gets $ linearly search $L$ for tetrahedron containing $\pos{p}$ (uses Algorithm \ref{alg:point_in_tet})\;
        \Return t

    \end{algorithm}
\end{minipage}

Our RTree approach results in checking significantly less tetrahedrons when performing the query. To verify the RTree's accuracy and measure its performance, we randomly generate a set of 200 points in a mesh with $\approx$1.1M tetrahedrons, which is a typical number from our benchmark meshes. For each random point, we perform both a linear and RTree lookup and ensure that both queries return the same tetrahedron (the linear query has already been rigorously verified). To benchmark performance, we measure the latency of each query. In total, the 200 linear and RTree queries take 18427ms and 115ms, respectively. The average linear query latency is 92ms while the average RTree query latency is 0.4ms. In summary, the RTree query achieves over a 230x speedup over the linear query. Due to Amdahl's law \cite{amdahl}, the RTree's effect on the overall performance of a simulation depends on the total time spent launching packets (the only place where the point-to-tetrahedron lookup is used), which can vary across models and simulation parameters. To quantify the RTree's effect on the simulators performance, we use a realistic model with 21 million tetrahedra and a single cylinder light source. We found that the RTree lookup resulted in a 10x speedup in total simulation time over the naive linear lookup.
