%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% COMMENT STARTS
\begin{comment}
\subsection{FullMonte}
The FullMonte algorithm \cite{cassidy-jbo} models the propagation of light through a tetrahedral mesh using the \textit{hop-drop-spin} method first proposed under a different name by Wilson and Adam in 1983 \cite{og_hop-drop-spin}. The method is visualized in Fig. \ref{fig:fm_pipeline}.

\input{figures/fullmonte_pipeline.tex}

\subsubsection*{Launch}
Light can be emitted from various sources such as: pencil beam, point, line, disk, tetrahedral volume, ball, fibre cone or a combination of sources. These light sources have been developed based on feedback from medical researchers to either represent an emitter exactly (e.g. the fiber cone) or allow more complicated sources to be created from multiple simpler sources. Regardless of the type of light source, an emitted packet is given an initial starting position \texttt{p}, direction vector \texttt{d} and \textit{weight} \texttt{w} of 1. It then moves to the \textsc{draw step} stage of the algorithm.

\subsubsection*{Draw Step}
Based on the scattering (\textmus) and absorption (\textmua) coefficients of the material in which the packet currently resides, it is \textit{stepped} by generating a random distance given by Equation \ref{eq:step}. Where \textmut{} is the attenuation coefficient (\textmut = \textmua + \textmus) and \texttt{r} is a uniform random variable \uniform{0}{1}.

\begin{equation} \label{eq:step}
    s = \frac{-log(r)}{\mu{}_{t}}
\end{equation}

\subsubsection*{Hop}
After the step length has been drawn the packet enters the \textsc{hop} stage where it is advanced by the drawn step length \texttt{s} in its current direction \texttt{d} (Equation \ref{eq:pos_update}). If the packet remains in the same tetrahedron after the step is finished, then the \textsc{hop} stage is complete and the packet moves to the \textsc{drop} stage. Otherwise it moves to the \textsc{interface} stage since the current hop caused the packet to cross a tetrahedral boundary.

\begin{equation} \label{eq:pos_update}
    p' = p + sd
\end{equation}

\subsubsection*{Interface}
If the packet crosses a tetrahedral boundary from tetrahedron \texttt{T} to \texttt{S} during the \textsc{hop} stage, then the intersection point on the shared face of \texttt{T} and \texttt{S} is calculated. If the refractive indices of \texttt{T} and \texttt{S} differ, then the packet crossed a material boundary and calculations must made to account for the interface. Based on the incident angle of the packet, Fresnel reflections, total internal reflection or refraction can cause the current direction of the packet to changed. If the attenuation coefficient \textmut{} of \texttt{T} and \texttt{S} differ, then the step length is updated by Equation \ref{eq:new_step} and the packet moves back to the \textsc{hop} stage with a new step length \texttt{s'}. If the packet exits the mesh, then it is marked as \textit{dead} and its execution ceases.
%In this case if the user chose to record mesh exit events then the weight of the packet is accumulated for the surface face the packet is crossing.

\begin{equation} \label{eq:new_step}
    s' = \frac{\mu{}_{t}}{\mu{}_{t}'}s
\end{equation}

\subsubsection*{Drop}
Once the step has finished the packet drops some of its \textit{energy} (i.e. weight) into the tetrahedron it resides in based on equations \ref{eq:drop_alpha}, \ref{eq:drop_delta} and \ref{eq:drop_remain}. If the user chose to record volume absorption events, then the dropped energy (Equation \ref{eq:drop_delta}) is accumulated in the current tetrahedron.

\begin{equation} \label{eq:drop_alpha}
    \alpha = \frac{\mu{}_{s}}{\mu{}_{s} + \mu{}_{a}}
\end{equation}
\begin{equation} \label{eq:drop_delta}
    \Delta{}w = (1 - \alpha)w
\end{equation}
\begin{equation} \label{eq:drop_remain}
    w' = \alpha{}w
\end{equation}

\subsubsection*{Packet Death (Roulette)}
As packets propagate and drop more of their weight they contribute less to the total absorption. To decrease the computational burden of tracking these increasingly insignificant packets we implement a termination process called \textit{roulette}. If after the \textsc{drop} stage a packet's weight is below \texttt{wmin} it is given a \textit{1-in-p} chance of surviving with an increased weight of \texttt{pw} (to conserve energy), otherwise it is terminated. If the packet's weight is greater than \textit{wmin} or if it survives roulette, then it moves to the \textsc{spin} stage.

\subsubsection*{Spin}
Materials that contain anisotropies intensify scattering and introduce complexity into modelling the precise scattering of photons. We use the Henyey-Greenstein (HG) function to model the \textit{cumulative} scattering behaviour of a material. The HG function uses a random azimuthal angle \textphi{} which is \uniform{0}{2\textpi} and deflection angle \texttheta{} described by Equation \ref{eq:hg} where \texttt{g} is the anisotropy of the material the packet currently resides in and \texttt{q} is \uniform{-1}{1}. The packet is \textit{spun} by having its direction vector \texttt{d} (and orthonormal vectors \texttt{a} an \texttt{b}) updated by Equation \ref{eqn:spin}. After it has been spun it moves back to the initial \textsc{draw step} stage where the process repeats.

\begin{equation} \label{eq:hg}
    cos\theta = \frac{1}{2g}\bigg[1 + g^{2} - \Big(\frac{1 - g^{2}}{1 + g - 2gq}\Big) \bigg]
\end{equation}

\begin{gather} \label{eqn:spin}
    \begin{bmatrix}
    d' \\
    a' \\
    b'
    \end{bmatrix}
    =
    \begin{bmatrix}
    cos\theta & -sin\theta{}cos\phi & sin\theta{}sin\phi \\
    sin\theta & cos\theta{}cos\phi & -cos\theta{}sin\phi \\
    0 & sin\phi & cos\phi \\
    \end{bmatrix}
    \begin{bmatrix}
    d \\
    a \\
    b
    \end{bmatrix}
\end{gather}
\end{comment}
%% COMMENT ENDS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%